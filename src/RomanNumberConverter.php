<?php

namespace Workshop;

/**
 * Class RomanNumberConverter
 *
 * This class can convert decimal numbers up to 3999 to Roman numbers.
 */
class RomanNumberConverter {

    const RANGE_0 = 0;
    const RANGE_1_3 = 1;
    const RANGE_4_8 = 2;
    const RANGE_9_10 = 3;

    const DIGIT_5 = 5;

    const ROMAN_NUMBERS_MAP = [
        0 => [
            self::RANGE_1_3 => 'I',
            self::RANGE_4_8 => 'V',
            self::RANGE_9_10 => 'X',
        ],
        1 => [
            self::RANGE_1_3 => 'X',
            self::RANGE_4_8 => 'L',
            self::RANGE_9_10 => 'C',
        ],
        2 => [
            self::RANGE_1_3 => 'C',
            self::RANGE_4_8 => 'D',
            self::RANGE_9_10 => 'M',
        ],
        3 => [
            self::RANGE_1_3 => 'M',
            self::RANGE_4_8 => '',
            self::RANGE_9_10 => '',
        ]
    ];



    /**
     * Convert decimal number to Roman number.
     *
     * @param int $decimal   The decimal number up to 3999
     *
     * @return string
     */
    public function convert($decimal) {
        $romanNumber = '';
        $decimal = (string) $decimal;
        $maxExponent = strlen($decimal) - 1;

        for ($i = 0; $i <= $maxExponent; $i++) {
            $exponent = $maxExponent - $i; // exponent of 10 for current digit's position.
            $romanNumber .= $this->convertSingleDigit($decimal[$i], $exponent);
        }

        return $romanNumber;

    }

    private function getNumberRange($number) {
        if (0 == $number) {
            return self::RANGE_0;
        } elseif ($number < 4) {
            return self::RANGE_1_3;
        } elseif ($number < 9) {
            return self::RANGE_4_8;
        } else {
            return self::RANGE_9_10;
        }
    }

    private function convertSingleDigit($number, $exponent) {
        $romanNumber = '';
        switch ($this->getNumberRange($number)) {
            case static::RANGE_0:
                $romanNumber = '';
                break;

            case static::RANGE_1_3:
                $romanNumber = $this->convertRange1($number, $exponent);
                break;

            case self::RANGE_4_8:
                $romanNumber = $this->convertRange5($number, $exponent);
                break;

            case self::RANGE_9_10:
                $romanNumber = $this->convertRange10($number, $exponent);
                break;

        }

        return $romanNumber;
    }

    private function convertRange1($number, $exponent) {
        return str_repeat(self::ROMAN_NUMBERS_MAP[$exponent][self::RANGE_1_3], $number);
    }

    private function convertRange5($number, $exponent) {
        $diff = $number - self::DIGIT_5;
        if ($diff > 0) {
            return self::ROMAN_NUMBERS_MAP[$exponent][self::RANGE_4_8] . $this->convertRange1(abs($diff), $exponent);
        } else {
            return $this->convertRange1(abs($diff), $exponent) . self::ROMAN_NUMBERS_MAP[$exponent][self::RANGE_4_8];
        }
    }

    private function convertRange10($number, $exponent) {
        return $this->convertRange1(1, $exponent) . self::ROMAN_NUMBERS_MAP[$exponent][self::RANGE_9_10];
    }
}
