<?php

namespace Test\Workshop;

use Workshop\RomanNumberConverter;

/**
 * Testing Decimal -> Roman number conversion.
 */
class RomanNumberConverterTest extends \PHPUnit_Framework_TestCase {

    /** @var RomanNumberConverter */
    private $converter;

    /**
     * Setup.
     */
    public function setUp()
    {
        $this->converter = new RomanNumberConverter();
    }

    /**
     * Run test on happy path...
     *
     * @param string $expectedRomanNumber
     * @param int $decimalNumber
     *
     * @dataProvider provideDecimalAndRomanNumberPairs
     */
    public function testConverterShouldConvertDecimalNumberToRoman($decimalNumber, $expectedRomanNumber) {


        $this->assertSame($expectedRomanNumber, $this->converter->convert($decimalNumber));
    }

    /**
     * Number data provider.
     *
     * @return array
     */
    public function provideDecimalAndRomanNumberPairs()
    {
        return [
            [0, ''],
            [1, 'I'],
            [2, 'II'],
            [3, 'III'],
            [4, 'IV'],
            [5, 'V'],
            [6, 'VI'],
            [7, 'VII'],
            [8, 'VIII'],
            [9, 'IX'],
            [10, 'X'],
            [11, 'XI'],
            [12, 'XII'],
            [13, 'XIII'],
            [14, 'XIV'],
            [15, 'XV'],
            [17, 'XVII'],
            [19, 'XIX'],
            [20, 'XX'],
            [41, 'XLI'],
            [87, 'LXXXVII'],
            [99, 'XCIX'],
            [100, 'C'],
            [897, 'DCCCXCVII'],
            [3999, 'MMMCMXCIX'],
        ];
    }
}
